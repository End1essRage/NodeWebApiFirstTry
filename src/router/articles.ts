import express from 'express';

import { getAllArticles, getOneArticleById, createNewArticle } from '../controllers/article';

export default (router: express.Router) => {
    router.get('/articles', getAllArticles);
    router.get('/articles/:id', getOneArticleById);
    router.post('/articles', createNewArticle);
};