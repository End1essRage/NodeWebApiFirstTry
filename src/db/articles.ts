import mongoose from "mongoose";

const ArticleSchema = new mongoose.Schema({
    topic: { type: String, required: true },
    body: { type: String, required: true },
    dateCreated: { type:Date },
});

export interface Article
{
    topic: string, 
    body: string,
    dateCreated?: Date
}

export const ArticleModel = mongoose.model('Article', ArticleSchema);

export const getArticles = () => ArticleModel.find();
export const getArticleById = (id: string) => ArticleModel.findById(id);
export const createArticle = (values: Record<string, any>) => new ArticleModel(values)
    .save().then((article) => article.toObject());
export const deleteArticleById = (id: string) => ArticleModel.findOneAndDelete({ _id: id });
export const updateArticleById = (id: string, values: Record<string, any>) => ArticleModel.findByIdAndUpdate({ id, values });