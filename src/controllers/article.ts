import express from 'express';

import { getArticles, getArticleById, createArticle, Article } from '../db/articles';

export const getAllArticles = async (req: express.Request, res: express.Response) => {
    try{
        const articles = await getArticles();

        return res.status(200).json(articles);
    }
    catch(error){
        console.log(error);
        return res.sendStatus(400);
    }
}

export const getOneArticleById = async (req: express.Request, res: express.Response) => {
    try{
        const {id}  = req.params;

        const article = await getArticleById(id);

        if(!article)
        {
            return res.sendStatus(404);
        }
            
        return res.json(article);
    }
    catch(error){
        console.log(error);
        return res.sendStatus(400);
    }
}

export const createNewArticle = async (req: express.Request, res: express.Response) => {
    try{
        const articleDto: Article = req.body;
        articleDto.dateCreated = new Date();
        const article = await createArticle(articleDto);
            
        return res.json(article);
    }
    catch(error){
        console.log(error);
        return res.sendStatus(400);
    }
}